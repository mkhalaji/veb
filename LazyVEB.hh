#include <cmath>
#include <cstdint>
#include <iostream>

using namespace std;

typedef uint64_t ui64;
typedef int64_t i64;

// debugging functions

class LazyVEB {
   public:
    i64 u;
    i64 lowerRoot;
    i64 upperRoot;
    i64 lowerPower;
    i64 upperPower;
    LazyVEB **clusters;
    LazyVEB *summary;
    volatile i64 min;
    volatile i64 max;


    // Lazy Constructor
    LazyVEB(i64 u) {
        this->u = u;
        this->min = -1;
        this->max = -1;
        this->lowerPower = floor(log2(u) / 2);
        this->lowerRoot = pow(2, lowerPower);
        this->upperPower = ceil(log2(u) / 2);
        this->upperRoot = pow(2, upperPower);

        if (u == 2) {

        } else {
            // lower square root
            // i64 clusterSize = lowerRoot;

            // upper square root
            // i64 nClusters = upperRoot;

            // this->summary = new LazyVEB(nClusters);
            this->summary = nullptr;

            // newing an array of nClusters pointers to VEB objects
            this->clusters = new LazyVEB * [upperRoot];

            for (i64 i = 0; i < upperRoot ; ++i) {
                this->clusters[i] = nullptr;
            }
        }
    }

    ~LazyVEB() {
        if (this->u > 2) {
            for (ui64 i = 0; i < this->upperRoot; ++i) {
                if (this->clusters[i] != nullptr)
                    delete this->clusters[i];
            }
            if (this->summary != nullptr)
                delete this->summary;
            delete[] this->clusters;
        }
    }


    // Constructor
    // VEB(i64 u) {
    //     this->u = u;
    //     this->min = -1;
    //     this->max = -1;
    //     this->lowerPower = floor(log2(u) / 2);
    //     this->lowerRoot = pow(2, lowerPower);
    //     this->upperPower = ceil(log2(u) / 2);
    //     this->upperRoot = pow(2, upperPower);

    //     if (u == 2) {
    //     } else {
    //         // lower square root
    //         i64 clusterSize = lowerRoot;

    //         // upper square root
    //         i64 nClusters = upperRoot;

    //         this->summary = new VEB(nClusters);

    //         // newing an array of nClusters pointers to VEB objects
    //         this->clusters = new VEB *[nClusters];

    //         for (i64 i = 0; i < nClusters; ++i) {
    //             this->clusters[i] = new VEB(clusterSize);
    //         }
    //     }
    // }

    void allocateClusterIfNeeded(i64 h) {
        if (this->clusters[h] == nullptr)
            this->clusters[h] = new LazyVEB(this->lowerRoot);
    }

    void allocateSummaryIfNeeded() {
        if (this->summary == nullptr)
            this->summary = new LazyVEB(this->upperRoot);
    }

    i64 low(i64 x) {
        // rightmost floor(lgu/2) bits
        return x & (this->lowerRoot - 1);
    }

    i64 high(i64 x) {
        // leftmost ceil(lgu/2) bits
        return x >> this->lowerPower;
    }

    i64 index(i64 cluster, i64 offset) {
        return cluster * this->lowerRoot + offset;
    }

    bool member(i64 x) {
        if (x == this->min || x == this->max) {
            return true;
        }

        if (this->u == 2) {
            // if it's neither min nor max, and we can't recurse any further, we're done
            return false;
        }
        i64 h = this->high(x);
        if (this->clusters[h] == nullptr)
            return false;
        return this->clusters[h]->member(this->low(x));
    }

    i64 maximum() {
        return this->max;
    }

    i64 minimum() {
        return this->min;
    }

    i64 successor(i64 x) {
        if (this->u == 2) {
            if (x == 0 && this->max == 1) {
                return 1;
            }
            return -1;
        }

        // the query is smaller than the minimum => return the minimum
        if (this->min != -1 && x < this->min) {
            return this->min;
        }

        i64 h = this->high(x);
        i64 l = this->low(x);
        i64 offset;

        // maximum in current cluster
        i64 maxLow = this->clusters[h]->max;

        // a maxLow exists, and it is bigger than our low
        // => a successor exists in current cluster
        if (maxLow != -1 && l < maxLow) {
            offset = this->clusters[h]->successor(l);
            return this->index(h, offset);
        }

        // maxLow doesn't exist
        // we should find the successor in the next clusters
        i64 succCluster = this->summary->successor(h);

        // nothing found in summary
        // => successor does not exist
        if (succCluster == -1) {
            return -1;
        }

        // succCluster exists, now find the minimum in there
        offset = this->clusters[succCluster]->min;

        return this->index(succCluster, offset);
    }

    i64 predecessor(i64 x) {
        if (this->u == 2) {
            if (x == 1 && this->min == 0) {
                return 0;
            }
            return -1;
        }

        // the query is bigger than the maximim => return the maximum
        if (this->max != -1 && x > this->max) {
            return this->max;
        }

        i64 h = this->high(x);
        i64 l = this->low(x);
        i64 offset;

        i64 minLow = this->clusters[h]->min;

        // a minLow exists, and it is smaller than our low
        // => a predecessor exists in current cluster
        if (minLow != -1 && minLow < l) {
            offset = this->clusters[h]->predecessor(l);
            return this->index(h, offset);
        }

        // minLow doesn't exist
        // we should find the predecessor in the previous clusters
        i64 predCluster = this->summary->predecessor(h);

        // nothing found in summary
        // THIS PART DIFFERS FROM THE SUCCESSOR IMPLEMENTATION
        if (predCluster == -1) {
            // predecessor might be stored in a min field somewhere
            // and we didn't see it because min is not stored in any clusters
            if (this->min != -1 && x > this->min) {
                return this->min;
            }

            // not found in summary, and minimum doesn't exist (or isn't less than x)
            return -1;
        }

        offset = this->clusters[predCluster]->max;
        return this->index(predCluster, offset);
    }

    void insertToEmptyVEB(i64 x) {
        this->min = x;
        this->max = x;
    }

    bool insert(i64 x) {
        // AVOID RE-INSERTING THE MINIMUM OR THE MAXIMUM INSIDE THE CLUSTER
        if (x == this->min || x == this->max) {
            return false;
        }

        // easy case: tree is empty
        if (this->min == -1) {
            this->insertToEmptyVEB(x);
            return true;
        }

        bool inserted;
        // x is our new minimum
        // set x as the new min, then insert the old min into the tree
        if (x < this->min) {

            // if it's less than the minimum, it doesn't exist in the tree
            // what about lower levels?
            // if it's smaller than the minimum in level l, it can't be greater than any of the minumums on level l + k
            // because the top level minimum is the absolute minimum
            inserted = true;

            i64 temp = x;
            x = this->min;
            this->min = temp;
        }

        if (this->u > 2) {
            i64 h = this->high(x);
            i64 l = this->low(x);
            this->allocateClusterIfNeeded(h);
            // the corresponding cluster is empty
            if (this->clusters[h]->min == -1) {
                this->allocateSummaryIfNeeded();
                this->summary->insert(h);
                this->clusters[h]->insertToEmptyVEB(l);
                inserted = true;
            }

            // the corresponding cluster already has some elements
            else {
                inserted = this->clusters[h]->insert(l);
            }
        }

        if (x > this->max) {
            // if it's greater than the maximum, it must be a new element, and we must have inserted it.
            // NOT NECESSARY, ALREADY TAKEN CARE OF BEFORE
            // inserted = true;

            this->max = x;
        }
        return inserted;
    }




    // this is supposed to not assume x already exists in V
    bool del(i64 x) {

        // significantly improves throughput
        if (x > this->max || x < this->min) {
            return false;
        }

        // there's only 1 element in V
        // rare
        if (this->min == this->max) {
            if (this->min == x) {
                this->min = -1;
                this->max = -1;
                return true;
            }
            // else {
                // there's only 1 element, and it's not x
                return false; 
            // }
        }


        // from now on, V has at least 2 elements
        if (this->u == 2) {
            // we know that V has 2 elems
            // we're at the base case
            // one of them must be x
            // delete it and set min and max accordingly            
            this->min = 1 - x;
            this->max = this->min;

            return true;
        }

        // from now on, V has at least 2 elements and u >= 4

        // if deleting the min value,
        // set one of the elems as the new min
        // delete that element from inside the cluster
        if (x == this->min) {
            // no need to do nullptr check on summary because if there are at least 2 elements in the structure, the summary has been allocated
            i64 firstCluster = this->summary->min;
            x = this->index(firstCluster, this->clusters[firstCluster]->min);
            this->min = x;
        }

        i64 h = this->high(x);
        
        // this if statement only, handles the case for lazy veb
        // (prevents from seg faults)
        // reclamation remaining though
        if (this->clusters[h] == nullptr) {
            return false;
        }

        i64 l = this->low(x);
        bool erased;
        // now delete x from the cluster
        erased = this->clusters[h]->del(l);

        // if successfully deleted x and the cluster is empty now
        if (this->clusters[h]->min == -1) {
            // free the memory
            delete this->clusters[h];
            this->clusters[h] = nullptr; // absence of this line causes segfaults

            // update summary so that it reflects the emptiness
            this->summary->del(h);

            

            // if we deleted the max element, we need to find the new max
            if (x == this->max) {
                i64 summaryMax = this->summary->max;

                // only 1 elem remaining
                if (summaryMax == -1) {
                    this->max = this->min;
                } else {
                    this->max = this->index(summaryMax, this->clusters[summaryMax]->max);
                }
            }

            if (this->summary->min == -1) {
                delete this->summary;
                this->summary = nullptr; // absence of this line causes segfaults
            }

        }

        // the cluster still has other elements after deleting x
        // we don't need to update the summary, but we might need to update the max
        // the erased check is not necessary
        // if it's equal to max, it's been deleted
        else if (x == this->max) {
            this->max = this->index(h, this->clusters[h]->max);
        }

        return erased;

    }

    i64 getSumOfKeys() {
        i64 res = 0;
        for (i64 i = 0; i < this->u; ++i) {
            if (this->member(i)) {
                res += i;
            }
        }

        return res;
    }

    bool same(LazyVEB *v) {
        if (this->min != v->min || this->max != v->max) {
            return false;
        }
        if (this->u == 2) {
            return true;
        } else {
            bool res = true;
            res = res && this->summary->same(v->summary);
            for (i64 i = 0; i < this->upperRoot; ++i) {
                res = res && this->clusters[i]->same(v->clusters[i]);
            }
            return res;
        }
    }

    // debugging func
    bool minMaxDiscrepancy() {
        if (this->min > this->max) {
            return true;
        }
        if (this->u > 2) {
            if (this->summary->minMaxDiscrepancy()) {
                return true;
            }
            // upper square root
            i64 nClusters = pow(2, ceil(log2(this->u) / 2));
            for (i64 i = 0; i < nClusters; ++i) {
                if (this->clusters[i]->minMaxDiscrepancy()) {
                    return true;
                }
            }
        }
        return false;
    }
};

// putting this outside of class definition because of vscode's debugger and compiler inlining
// https://stackoverflow.com/questions/22163730/cannot-evaluate-function-may-be-inlined
// void printTrivia(VEB *veb) {
//     // lower square root
//     i64 clusterSize = pow(2, floor(log2(veb->u) / 2));
//     // upper square root
//     i64 nClusters = pow(2, ceil(log2(veb->u) / 2));

//     cout << "Universe size: " << veb->u << endl;
//     cout << nClusters << " clusters of size " << clusterSize << endl;
//     cout << "Minimum: " << veb->min << endl;
//     cout << "Maximum: " << veb->max << endl;
//     i64 clusterNo;
//     for (i64 i = 0; i < veb->u; ++i) {
//         clusterNo = floor(i / clusterSize);
//         if (i % clusterSize == 0) {
//             cout << "\n\n********* CLUSTER " << clusterNo << " {Min: " << veb->clusters[clusterNo]->min << "} {Max: " << veb->clusters[clusterNo]->max << "}"
//                  << " *********\n\n";
//         }
//         if (veb->member(i)) {
//             cout << i << " [low=" << veb->low(i) << "]"
//                  << " - ";
//         }
//     }
//     cout << "" << endl;
//     cout << "-------------------------------------------------------------------------------" << endl;
//     cout << "" << endl;
// }
