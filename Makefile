GPP = g++
FLAGS = -O3 -g
FLAGS += -std=c++17
FLAGS += -fopenmp
LDFLAGS = -pthread

all: benchmark benchmark_debug

.PHONY: benchmark
benchmark:
	$(GPP) $(FLAGS) -o $@.out benchmark.cpp $(LDFLAGS) -DNDEBUG #### NOTE: THIS DISABLES ASSERTIONS!!!

.PHONY: benchmark2
benchmark2:
	$(GPP) $(FLAGS) -o $@.out benchmark2.cpp $(LDFLAGS) -DNDEBUG #### NOTE: THIS DISABLES ASSERTIONS!!!

.PHONY: benchmark_debug
benchmark_debug:
	$(GPP) $(FLAGS) -o $@.out benchmark.cpp -DTRACE=if\(1\) $(LDFLAGS)

.PHONY: main
main: 
	$(GPP) $(FLAGS) -o $@.out main.cpp -DTRACE=if\(1\) $(LDFLAGS)

.PHONY: correctnessTest
correctnessTest: 
	$(GPP) $(FLAGS) -o $@.out correctnessTest.cpp -DTRACE=if\(1\) $(LDFLAGS)

clean:
	rm -f *.out 
