#include <stdlib.h> /* srand, rand */

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <functional>
#include <iostream>
#include <random>
#include <set>
#include <vector>

#include "VEB.hh"

using namespace std;

typedef uint64_t ui64;
typedef uint32_t ui32;
typedef int64_t i64;

i64 chooseRandomIdx(vector<i64> &src) {
    return rand() % src.size();
}

void move(vector<i64> &src, vector<i64> &dest, i64 randIdx) {
    dest.push_back(src[randIdx]);
    auto it = src.begin();
    for (i64 i = 0; i < randIdx; ++i) {
        it++;
    }
    src.erase(it);
}

bool matchQueries(VEB *veb, set<i64> stdSet) {
    i64 setValue, vebValue;
    i64 u = veb->u;

    // minimum
    setValue = stdSet.size() > 0 ? *stdSet.begin() : -1;
    vebValue = veb->minimum();

    if (setValue != vebValue) {
        return false;
    }

    // maximum
    setValue = stdSet.size() > 0 ? *stdSet.rbegin() : -1;
    vebValue = veb->maximum();

    if (setValue != vebValue) {
        return false;
    }

    for (i64 i = 0; i < u; ++i) {
        // membership
        setValue = stdSet.find(i) != stdSet.end();
        vebValue = veb->member(i);

        if (setValue != vebValue) {
            return false;
        }

        // predecessor
        if (upper_bound(stdSet.rbegin(), stdSet.rend(), i, greater<i64>()) == stdSet.rend()) {
            setValue = -1;
        } else {
            setValue = *upper_bound(stdSet.rbegin(), stdSet.rend(), i, greater<i64>());
        }

        vebValue = veb->predecessor(i);

        if (setValue != vebValue) {
            return false;
        }

        // successor
        if (stdSet.upper_bound(i) != stdSet.end()) {
            setValue = *stdSet.upper_bound(i);
        } else {
            setValue = -1;
        }
        vebValue = veb->successor(i);

        if (setValue != vebValue) {
            return false;
        }
    }
    return true;
}

int main() {

    const int NUM_TRIALS = 1;
    const int OPERATIONS_PER_TRIAL = 500000;
    const int STARTING_POWER = 10;  // the power of two that we use for our veb size
    const int POWER_RANGE = 13;
    const float INSERT_PROB = 0.5;       // insert to delete ratios
    const float DELETE_EXISTING_PROB = 0.75;  // probability of attempting to delete an element that actually exists

    // in first glance, this might seem redundant, but it saved my life :)
    // more details in the insert function of the veb class
    const float INSERT_EXISTING_PROB = 0.2;  // probability of attempting to insert an element that already exists
    const int PREFILL_DENOMINATOR = 2;

    int power;
    i64 u;

    set<i64> stdSet;
    vector<i64> notInserted;
    vector<i64> inserted;
    std::random_device rd;   // Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd());  // Standard mersenne_twister_engine seeded with rd()
    uniform_real_distribution<> urd(0.0, 1.0);

    i64 attemptedInserts, inserts, attemptedDeletes, deletes;
    i64 stdSetSum, vebSum, insertedSum;

    for (int i = 0; i < NUM_TRIALS; ++i) {
        attemptedInserts = 0;
        inserts = 0;
        attemptedDeletes = 0;
        deletes = 0;
        vebSum = 0;
        stdSetSum = 0;
        insertedSum = 0;

        power = 24;
        u = pow(2, power);
        cout << "Testing with universe size " << u << endl;
        auto veb = new VEB(u);
        stdSet.clear();
        notInserted.clear();
        inserted.clear();

        for (i64 j = 0; j < u; ++j) {
            notInserted.push_back(j);
        }

        // PREFILL
        i64 randIdx;
        i64 key;
        i64 prefillSize = 5000;
        for (i64 j = 0; j < prefillSize; ++j) {
            // if (veb->minMaxDiscrepancy()) {
            // cout << "DISCREPANCY" << endl;
            // }
            if (j % 1000 == 0) {
                cout << "prefilling " << j << endl;
            }
            randIdx = chooseRandomIdx(notInserted);
            key = notInserted[randIdx];
            move(notInserted, inserted, randIdx);
            veb->insert(key);
            stdSet.insert(key);
            // cout << "prefill inserted " << key << endl;
        }

        for (i64 j = 0; j < OPERATIONS_PER_TRIAL; ++j) {
            if (veb->minMaxDiscrepancy()) {
                cout << "DISCREPANCY" << endl;
            }
            if (urd(gen) <= INSERT_PROB) {
                attemptedInserts++;
                // insert
                if (urd(gen) <= INSERT_EXISTING_PROB && inserted.size() > 0) {
                    // "attempt" to insert an element that already exists
                    // cout << "inserting existing" << endl;
                    randIdx = chooseRandomIdx(inserted);
                    key = inserted[randIdx];
                    veb->insert(key);
                    stdSet.insert(key);
                    // cout << "fake inserted " << key << endl;
                } else if (notInserted.size() > 0) {
                    // insert an element that doesn't exist
                    inserts++;
                    randIdx = chooseRandomIdx(notInserted);
                    key = notInserted[randIdx];
                    move(notInserted, inserted, randIdx);
                    veb->insert(key);
                    stdSet.insert(key);
                    // cout << "inserted " << key << endl;
                }
            } else {
                // delete
                attemptedDeletes++;
                if (urd(gen) <= DELETE_EXISTING_PROB && inserted.size() > 0) {
                    // delete an element that actually exists
                    deletes++;
                    randIdx = chooseRandomIdx(inserted);
                    key = inserted[randIdx];
                    move(inserted, notInserted, randIdx);
                    veb->del(key);
                    stdSet.erase(key);
                    // cout << "deleted " << key << endl;
                } else if (notInserted.size() > 0) {
                    // "attempt" to delete an element that doesn't exist
                    randIdx = chooseRandomIdx(notInserted);
                    key = notInserted[randIdx];
                    veb->del(key);
                    stdSet.erase(key);
                }
            }
        }

        for (auto it = inserted.begin(); it != inserted.end(); it++) {
            insertedSum += *it;
        }
        for (auto it = stdSet.begin(); it != stdSet.end(); it++) {
            stdSetSum += *it;
        }
        for (i64 j = 0; j < u; ++j) {
            if (veb->member(j)) {
                vebSum += j;
            }
        }

        cout << "Actual Inserts: " << inserts << endl;
        cout << "'Fake' Inserts: " << (attemptedInserts - inserts) << endl;
        cout << "Actual Deletes: " << deletes << endl;
        cout << "'Fake' Deletes: " << (attemptedDeletes - deletes) << endl;
        cout << "'Inserted' Vector Values Sum: " << insertedSum << endl;
        cout << "std::set Values Sum: " << stdSetSum << endl;
        cout << "My VEB Values Sum: " << vebSum << endl;
        cout << "Sums Match: " << (stdSetSum == vebSum ? "Yes" : "No") << endl;
        cout << "Membership, Min, Max, Pred, Succ Match: " << (matchQueries(veb, stdSet) ? "Yes" : "No") << endl;
        cout << "Min-Max Discrepancy (Min Greater than Max): " << (veb->minMaxDiscrepancy() ? "Yes" : "No") << endl;
        cout << "-----------------------------------------------------" << endl;
    }
}