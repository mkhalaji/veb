#include <iostream>

#include "protoVEB.hh"
#include "VEB.hh"

using namespace std;

int main() {
    cout << "hello world" << endl;
    VEB *veb = new VEB(8);

    auto res = veb->insert(5);
    cout << "inserted 5: " << res << endl; 
    res = veb->insert(6);
    cout << "inserted 6: " << res << endl;
    res = veb->insert(5);
    cout << "inserted 5: " << res << endl;
    // auto res = veb2->insertWithPeaceOfMindAndReturnBoolean(5);
    // cout << res << endl;
    // veb2->insertWithPeaceOfMindAndReturnBoolean(6);
    // res = veb2->insertWithPeaceOfMindAndReturnBoolean(5);
    

    for (int i = 0; i < 8; ++i) {
        cout << i << ": member: {" << veb->member(i) << "} predecessor: {" << veb->predecessor(i) << "} successor: {" << veb->successor(i) << "}" << endl;
    }


    // veb->insertWithPeaceOfMind(5);
    // printTrivia(veb);
    // // veb->insertNonExistentKey(4);
    // // veb->insertNonExistentKey(6);
    // veb->insertWithPeaceOfMind(5);
    // printTrivia(veb);
    // // veb->insertNonExistentKey(10);
    // // veb->insertNonExistentKey(5);


    // veb->insertWithPeaceOfMind(5);
    // veb->insertWithPeaceOfMind(15);
    // veb->insertWithPeaceOfMind(10);

    // cout << "min: " << veb->min << ", max: " <<  veb->max << endl;
    // printTrivia(veb);

    // veb->insertWithPeaceOfMind(5);
    // veb->insertWithPeaceOfMind(10);

    // veb->insertWithPeaceOfMind(15);

    // cout << "min: " << veb->min << ", max: " <<  veb->max << endl;
    // printTrivia(veb);
    // veb->insert(6);
    // veb->insert(10);
    // veb->insert(29);
    // cout << "minimum: " << veb->minimum() << endl;
    // cout << "maximum: " << veb->maximum() << endl;
    // cout << "successor of 6: " << veb->successor(6) << endl;5
    // cout << "predecessor of 6: " << veb->predecessor(6) << endl;
    // cout << "predecessor of 7: " << veb->predecessor(7) << endl;
    // cout << "successor of 11: " << veb->successor(11) << endl;
    // cout << "predecessor of 4: " << veb->predecessor(4) << endl;
    // cout << "deleting 5" << endl;
    // veb->del(5);
    // cout << "minimum: " << veb->minimum() << endl;
    // cout << "maximum: " << veb->maximum() << endl;
    // cout << "successor of 6: " << veb->successor(6) << endl;
    // cout << "predecessor of 6: " << veb->predecessor(6) << endl;
    // cout << "predecessor of 7: " << veb->predecessor(7) << endl;
    // cout << "successor of 11: " << veb->successor(11) << endl;
    // cout << "predecessor of 4: " << veb->predecessor(4) << endl;

    return 0;
}