$!

rangeBegin=$1
rangeEnd=$2
trials=$3
millis=$4
outFile=$5

for (( pow=$rangeBegin; pow<=$rangeEnd; pow++ ))
do
    for (( trial=1; trial<=$trials; trial++ ))
    do
        for alg in set veb
        do
            echo $pow $alg `./benchmark.out -a $alg -p $pow -m $millis -t 1 -n $trial | grep -E "^throughput" | cut -d ":" -f 2 | xargs` | tee -a "$outFile.txt"
        done;
    done;
done;
