#include <cmath>
#include <cstdint>
#include <iostream>

using namespace std;

typedef uint64_t ui64;
typedef uint32_t ui32;
typedef int64_t i64;

class ProtoVEB {
   public:
    ui32 u;
    ProtoVEB **clusters;
    ProtoVEB *summary;
    bool A[2];

    // Constructor
    ProtoVEB(ui32 _u) {
        u = _u;
        if (u == 2) {
            A[0] = false;
            A[1] = false;
        } else {
            // lower square root
            ui32 clusterSize = pow(2, floor(log2(u) / 2));

            // upper square root
            ui32 nClusters = pow(2, ceil(log2(u) / 2));

            summary = new ProtoVEB(nClusters);

            // newing an array of nClusters pointers to protoVEB objects
            clusters = new ProtoVEB*[nClusters];

            for (ui32 i = 0; i < nClusters; ++i) {
                clusters[i] = new ProtoVEB(clusterSize);
            }
        }
    }

    ui32 low(ProtoVEB *V, ui32 x) {
        // rightmost floor(lgu/2) bits
        ui32 mask = pow(2, floor(log2(V->u) / 2)) - 1;
        return x & mask;
    }

    ui32 high(ProtoVEB *V, ui32 x) {
        // rightmost ceil(lgu/2) bits
        ui32 b = floor(log2(V->u) / 2);
        return x >> b;
    }

    ui32 index(ProtoVEB *V, ui32 cluster, ui32 offset) {
        ui32 lowerRoot = pow(2, floor(log2(V->u) / 2));
        return cluster * lowerRoot + offset;
    }

    bool member(ProtoVEB *V, ui32 x) {
        if (V->u == 2) {
            return V->A[x] == true;
        }
        return member(V->clusters[high(V, x)], low(V, x));
    }

    i64 maximum(ProtoVEB *V) {
        if (V->u == 2) {
            if (V->A[1]) {
                return 1;
            }
            if (V->A[0]) {
                return 0;
            }
            return -1;
        }
        ui32 maxCluster = maximum(V->summary);
        if (maxCluster == -1) {
            return -1;
        }
        ui32 offset = maximum(V->clusters[maxCluster]);
        return index(V, maxCluster, offset);
    }

    i64 minimum(ProtoVEB *V) {
        if (V->u == 2) {
            if (V->A[0]) {
                return 0;
            }
            if (V->A[1]) {
                return 1;
            }
            return -1;
        }
        ui32 minCluster = minimum(V->summary);
        if (minCluster == -1) {
            return -1;
        }
        ui32 offset = minimum(V->clusters[minCluster]);
        return index(V, minCluster, offset);
    }

    i64 successor(ProtoVEB *V, ui32 x) {
        if (V->u == 2) {
            if (x == 0 && V->A[1]) {
                return 1;
            }
            return -1;
        }
        ui32 h = high(V, x);
        ui32 l = low(V, x);
        ui32 offset = successor(V->clusters[h], l);
        if (offset != -1) {
            return index(V, h, offset);
        }
        ui32 successorsCluster = successor(V->summary, h);
        if (successorsCluster == -1) {
            return -1;
        }
        offset = minimum(V->clusters[successorsCluster]);
        return index(V, successorsCluster, offset);
    }

    i64 predecessor(ProtoVEB *V, ui32 x) {
        if (V->u == 2) {
            if (x == 1 && V->A[0]) {
                return 0;
            }
            return -1;
        }
        ui32 h = high(V, x);
        ui32 l = low(V, x);
        ui32 offset = predecessor(V->clusters[h], l);
        if (offset != -1) {
            return index(V, h, offset);
        }
        ui32 predecessorsCluster = predecessor(V->summary, h);
        if (predecessorsCluster == -1) {
            return -1;
        }
        offset = maximum(V->clusters[predecessorsCluster]);
        return index(V, predecessorsCluster, offset);
    }

    void insert(ProtoVEB *V, ui32 x) {
        if (V->u == 2) {
            V->A[x] = true;
            return;
        }

        ui32 h = high(V, x);
        ui32 l = low(V, x);
        insert(V->clusters[h], l);
        insert(V->summary, h);
    }

    void del(ProtoVEB *V, ui32 x) {
        if (V->u == 2) {
            V->A[x] = false;
            return;
        }
        ui64 h = high(V, x);
        ui64 l = low(V, x);
        del(V->clusters[h], l);

        bool othersInCluster = false;
        ui32 lowerRoot = pow(2, floor(log2(V->u) / 2));
        for(ui32 i = 0; i < lowerRoot; ++i) {
            if (member(V->clusters[h], i)) {
                othersInCluster = true;
                break;
            }
        }

        if (!othersInCluster) {
            del(V->summary, h);
        }
    }
};