#include <cmath>
#include <cstdint>
#include <iostream>
#include <set>
#include <functional>
#include <algorithm>
using namespace std;

typedef uint64_t ui64;
typedef int64_t i64;

class SetWrapper {
   public:
    set<i64> stdSet;
    i64 u;

    SetWrapper(i64 _u) {
        u = u;
    }

    bool member(i64 x) {
        if (stdSet.find(x) != stdSet.end()) {
            return true;
        }
        return false;
    }

    i64 minimum() {
        return stdSet.size() > 0 ? *stdSet.begin() : -1;
    }

    i64 maximum() {
        return stdSet.size() > 0 ? *stdSet.rbegin() : -1;
    }

    i64 successor(i64 x) {
        if (stdSet.upper_bound(x) != stdSet.end()) {
            return *stdSet.upper_bound(x);
        } else {
            return -1;
        }
    }

    i64 predecessor(i64 x) {
        if (upper_bound(stdSet.rbegin(), stdSet.rend(), x, greater<i64>()) == stdSet.rend()) {
            return -1;
        } else {
            return *upper_bound(stdSet.rbegin(), stdSet.rend(), x, greater<i64>());
        }
    }

    bool insert(i64 x) {
        auto p = stdSet.insert(x);
        return p.second;
    }

    bool del(i64 x) {
        auto numErased = stdSet.erase(x);
        return numErased == 1;
    }

    i64 getSumOfKeys() {
        i64 res = 0;
        auto it = stdSet.begin();
        while (it != stdSet.end()) {
            res += *it;
            it++;
        }
        return res;
    }
};